﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Models
{
    public class Ingreso
    {
        public int Id { get; set; }
        public int CuentaId { get; set; }
        public DateTime FechaHora { get; set; }
        public string Descripcion { get; set; }
        public double Monto { get; set; }
    }
}
