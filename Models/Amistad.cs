﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Models
{
    public class Amistad
    {
        public int Id { get; set; }
        public int Amigo1 { get; set; }
        public int Amigo2 { get; set; }
        public bool Activo { get; set; }

    }
}
