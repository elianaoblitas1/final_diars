﻿using Examen_Final_DIARS.Models.DB.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Models.DB
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        //Dbsets
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Cuenta> Cuentas { get; set; }
        public DbSet<Gasto> Gastos { get; set; }
        public DbSet<Ingreso> Ingresos { get; set; }
        public DbSet<Transaccion> Transaccions { get; set; }
        public DbSet<Amistad> Amistads { get; set; }
        //configuraciones de tablas
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UsuarioConfigurations());
            modelBuilder.ApplyConfiguration(new CuentaConfigurations());
            modelBuilder.ApplyConfiguration(new GastoConfigurations());
            modelBuilder.ApplyConfiguration(new IngresoConfigurations());
            modelBuilder.ApplyConfiguration(new TransaccionConfigurations());
            modelBuilder.ApplyConfiguration(new AmistadConfigurations());
        }

    }
}
