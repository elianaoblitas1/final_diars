﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Models.DB.Configurations
{
    public class AmistadConfigurations : IEntityTypeConfiguration<Amistad>
    {
        public void Configure(EntityTypeBuilder<Amistad> builder)
        {
            builder.ToTable("Amistad");
            builder.HasKey(o=>o.Id);
        }
    }
}
