﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Models
{
    public class Transaccion
    {
        public int Id { get; set; }
        public int UsuarioOId { get; set; }
        public int UsuarioDId { get; set; }
        public int CuentaDId { get; set; }
        public double Monto { get; set; }
        public int  CuentaAId { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
    }
}
