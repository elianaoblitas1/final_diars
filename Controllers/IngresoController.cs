﻿using Examen_Final_DIARS.Models;
using Examen_Final_DIARS.Models.DB;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Controllers
{
    public class IngresoController : Controller { 

        private AppDbContext context;
    
        public IngresoController(AppDbContext _context)
        {
            context = _context;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Create(int id_cuenta)
        {
            var ingreso = new Ingreso();
            ingreso.CuentaId = id_cuenta;
            return View(ingreso);
        }
        [HttpPost]
        public IActionResult Create(Ingreso ingreso)
        {
            if (ModelState.IsValid)
            {
                var cuenta = context.Cuentas.Where(o => o.Id == ingreso.CuentaId).First();
                cuenta.SaldoIni = cuenta.SaldoIni + ingreso.Monto;
                context.SaveChanges();
                context.Ingresos.Add(ingreso);
                context.SaveChanges();
                return RedirectToAction("Index", "Cuenta", new { id_cuenta = cuenta.Id });
            }

            return View(ingreso);
        }
    
    }
}
