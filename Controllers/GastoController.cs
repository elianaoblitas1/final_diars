﻿using Examen_Final_DIARS.Models;
using Examen_Final_DIARS.Models.DB;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Controllers
{
    public class GastoController : Controller
    {
        private AppDbContext context;
        public GastoController(AppDbContext _context)
        {
            context = _context;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Create(int id_cuenta)
        {
            var gasto = new Gasto();
            gasto.CuentaId = id_cuenta;
            return View(gasto);
        }
        [HttpPost]
        public IActionResult Create(Gasto gasto)
        {
            if (ModelState.IsValid)
            {
                var cuenta = context.Cuentas.Where(o => o.Id == gasto.CuentaId).First();
                cuenta.SaldoIni = cuenta.SaldoIni - gasto.Monto;
                context.SaveChanges();
                context.Gastos.Add(gasto);
                context.SaveChanges();
                return RedirectToAction("Index", "Cuenta", new { id_cuenta = cuenta.Id });
            }
                
            return View(gasto);
        }
    }
}
