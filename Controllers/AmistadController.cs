﻿using Examen_Final_DIARS.Extensions;
using Examen_Final_DIARS.Models;
using Examen_Final_DIARS.Models.DB;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Controllers
{
    public class AmistadController : Controller
    {
        private AppDbContext context;
        public AmistadController(AppDbContext _context)
        {
            context = _context;
        }
        public IActionResult Index()
        {
            var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
            var amistades = context.Amistads.Where(o => o.Amigo1 == usuario.Id || o.Amigo2 == usuario.Id).Where(o => o.Activo == true).ToList();
            var solicitudes = context.Amistads.Where(o => o.Amigo2 == usuario.Id && o.Activo == false).ToList();
            List<Usuario> usuariosS = new List<Usuario>();
            foreach (var item in solicitudes)
            {
                var amigo = context.Usuarios.Where(o => o.Id == item.Amigo1).First();
                usuariosS.Add(amigo);
            }
            ViewBag.solicitudes = usuariosS;
            List<Usuario> usuarios = new List<Usuario>();
            foreach (var item in amistades)
            {
                if (item.Amigo1 ==usuario.Id)
                {
                    var amigo = context.Usuarios.Where(o=>o.Id == item.Amigo2).First();
                    usuarios.Add(amigo);
                }
                else
                {
                    var amigo = context.Usuarios.Where(o => o.Id == item.Amigo1).First();
                    usuarios.Add(amigo);
                }
            }
            return View(usuarios);
        }
        [HttpGet]
        public IActionResult Create()
        {
            var personas = context.Usuarios.ToList();
            ViewBag.personas = personas;
            var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
            var amistad = new Amistad();
            amistad.Amigo1 = usuario.Id;
            return View(amistad);
        }
        [HttpPost]
        public IActionResult Create(Amistad amistad)
        {
            amistad.Activo = false;
            if (ModelState.IsValid)
            {
                context.Amistads.Add(amistad);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(amistad);
        }
        [HttpGet]
        public IActionResult Aceptar(int id_amigo)
        {
            var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
            var amistad = context.Amistads.Where(o => o.Amigo1 == id_amigo && o.Amigo2 == usuario.Id).First();
            amistad.Activo = true;
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Transaccion(int id_amigo)
        {
            var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
            var cuentas = context.Cuentas.Where(o=>o.UsuarioId == usuario.Id).ToList();
            ViewBag.cuentas = cuentas;
            var cuentaAmigo = context.Cuentas.Where(o => o.UsuarioId == id_amigo).First();
            var transaccion = new Transaccion();
            transaccion.CuentaAId = cuentaAmigo.Id;
            transaccion.UsuarioDId = id_amigo;
            transaccion.UsuarioOId = usuario.Id;
            return View(transaccion);
        }
        [HttpPost]
        public IActionResult Transaccion(Transaccion transaccion)
        {
            transaccion.Fecha = DateTime.Now;
            if (ModelState.IsValid)
            {
                var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
                var cuentaAmigo = context.Cuentas.Where(o=>o.Id == transaccion.CuentaAId).First();
                cuentaAmigo.SaldoIni = cuentaAmigo.SaldoIni + transaccion.Monto;
                context.SaveChanges();
                var cuentaUsuario = context.Cuentas.Where(o => o.Id == transaccion.CuentaDId).First();
                cuentaUsuario.SaldoIni = cuentaUsuario.SaldoIni - transaccion.Monto;
                context.SaveChanges();
                context.Transaccions.Add(transaccion);
                context.SaveChanges();
                return RedirectToAction("Index", "Usuario");
            }
            return View(transaccion);
        }

    }
}
