﻿using Examen_Final_DIARS.Extensions;
using Examen_Final_DIARS.Models;
using Examen_Final_DIARS.Models.DB;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Controllers
{
    public class UsuarioController : Controller
    {
        private AppDbContext context;
        public UsuarioController(AppDbContext _context)
        {
            context = _context;
        }

        public IActionResult Index()
        {
            var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
            var cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            var saldoTotal = 0.0;
            foreach (var cuenta in cuentas)
            {
                if (cuenta.Categoria == "Propio")
                {
                    saldoTotal = saldoTotal + cuenta.SaldoIni;
                }
            }
            ViewBag.saldoTotal = saldoTotal;
            var transacciones = context.Transaccions.Where(o => o.UsuarioDId == usuario.Id || o.UsuarioOId == usuario.Id).ToList();
            ViewBag.transacciones = transacciones;
            return View(cuentas);
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string nombre, string pass)
        {
            var usuario = context.Usuarios.FirstOrDefault(o => o.Nombre == nombre && o.Password == pass);
            if (usuario == null)
            {
                return View();
            }
            HttpContext.Session.Set("LoggedUser", usuario);
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name,usuario.Nombre),
            };
            var userIdentity = new ClaimsIdentity(claims, "login");
            var principal = new ClaimsPrincipal(userIdentity);
            HttpContext.SignInAsync(principal);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Registro()
        {
            return View(new Usuario());
        }
        [HttpPost]
        public IActionResult Registro(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                context.Usuarios.Add(usuario);
                context.SaveChanges();
                return RedirectToAction("Login");
            }

            return View(usuario);
        }

    }
}
