﻿using Examen_Final_DIARS.Extensions;
using Examen_Final_DIARS.Models;
using Examen_Final_DIARS.Models.DB;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Controllers
{
    public class TransaccionController : Controller
    {
        private AppDbContext context;

        public TransaccionController(AppDbContext _context)
        {
            context = _context;
        }
        [HttpGet]
        public IActionResult Create()
        {
            var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
            var cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            ViewBag.cuentas = cuentas;
            var transaccion = new Transaccion();
            transaccion.UsuarioOId = usuario.Id;
            transaccion.UsuarioDId = usuario.Id;
            return View(transaccion);
        }
        [HttpPost]
        public IActionResult Create(Transaccion transaccion)
        {
            transaccion.Fecha = DateTime.Now;
            if (ModelState.IsValid)
            {
                transaccion.Descripcion = "TRANSACCION";
                var cuentad = context.Cuentas.Where(o=>o.Id==transaccion.CuentaDId).First();
                cuentad.SaldoIni = cuentad.SaldoIni - transaccion.Monto;
                context.SaveChanges();
                var cuentaa = context.Cuentas.Where(o => o.Id == transaccion.CuentaAId).First();
                cuentaa.SaldoIni = cuentaa.SaldoIni + transaccion.Monto;
                context.SaveChanges();
                context.Transaccions.Add(transaccion);
                context.SaveChanges();
                return RedirectToAction("Index", "Usuario");
            }
            return View(transaccion);
        }
    }
}
