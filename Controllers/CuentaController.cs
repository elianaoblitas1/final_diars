﻿using Examen_Final_DIARS.Extensions;
using Examen_Final_DIARS.Models;
using Examen_Final_DIARS.Models.DB;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Controllers
{
    public class CuentaController : Controller
    {
        private AppDbContext context;
        public CuentaController(AppDbContext _context)
        {
            context = _context;
        }
        public IActionResult Index(int id_cuenta)
        {
            var cuenta = context.Cuentas.Where(o => o.Id == id_cuenta).First();
            var ingresos = context.Ingresos.Where(o => o.CuentaId == cuenta.Id);
            var gastos = context.Gastos.Where(o => o.CuentaId == cuenta.Id);
            ViewBag.ingresos = ingresos;
            ViewBag.gastos = gastos;
            return View(cuenta);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View(new Cuenta());
        }
        [HttpPost]
        public IActionResult Create(Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
                cuenta.UsuarioId = usuario.Id;
                context.Cuentas.Add(cuenta);
                context.SaveChanges();
                return RedirectToAction("Index","Usuario");
            }
            return View(cuenta);
        }
    }
}
